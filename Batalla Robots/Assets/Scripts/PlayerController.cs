﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public bool m_Player1;
    public float m_MaxSpeed;

    private Animator Animator;
    private Rigidbody rb;
    private bool Walk;
    private float Speed;
    private bool Attack;
    private bool Defend;

    void Start()
    {
        Animator = gameObject.GetComponent<Animator>();
        rb = gameObject.GetComponent<Rigidbody>();
        Animator.speed = 1.5f;
        Walk = false;
        Attack = false;
        Defend = false;
    }

    void Update()
    {
        UpdateState();
        UpdateAnimator();
    }

    void FixedUpdate()
    {
        if(Walk)
        {
            rb.velocity = new Vector3(0, 0, Speed);
        } else
        {
            rb.velocity = new Vector3(0, 0, 0);
        }
    }

    private void UpdateState()
    {
        bool AttackPressed = Input.GetKeyDown(KeyCode.F) && m_Player1 || Input.GetKeyDown(KeyCode.H) && !m_Player1;
        bool DefendPressed = Input.GetKeyDown(KeyCode.R) && m_Player1 || Input.GetKeyDown(KeyCode.Y) && !m_Player1;

        if (Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack") && Attack && !AttackPressed)
        {
            Attack = false;
        }

        if (Animator.GetCurrentAnimatorStateInfo(0).IsName("Defend") && Defend && !DefendPressed)
        {
            Defend = false;
        }

        if (AttackPressed)
        {
            //ATTACK
            Attack = true;
            Walk = false;
            Defend = false;
        }
        else if (DefendPressed)
        {
            //DEFEND
            Defend = true;
            Walk = false;
            Attack = false;
        }
        else if (Input.GetKey(KeyCode.A) && m_Player1 || Input.GetKey(KeyCode.J) && !m_Player1)
        {
            //FORWARD
            Walk = true;
            Attack = false;
            Defend = false;
            Speed = m_MaxSpeed;
        }
        else if (Input.GetKey(KeyCode.D) && m_Player1 || Input.GetKey(KeyCode.L) && !m_Player1)
        {
            //BACKWARD
            Walk = true;
            Attack = false;
            Defend = false;
            Speed = -m_MaxSpeed;
        } else
        {
            Walk = false;
        }
    }

    private void UpdateAnimator()
    {
        Animator.SetBool("Attack", Attack);
        Animator.SetBool("Defend", Defend);
        Animator.SetBool("Walk", Walk);
    }
}
